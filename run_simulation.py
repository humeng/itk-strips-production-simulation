import os
import random
import simpy
import model
import endcap
import barrel
import math
import csv
import matplotlib
import configparser

env = simpy.Environment()


config = configparser.ConfigParser()
config.read('config.ini')

# Set up raw part suppliers

## Endcap

CarltonSensor = endcap.SensorTestSite(env, 'CarltonSensor')
CarltonSensor.test_rate = float(config['Sensors']['canada_test_rate'])
CarltonSensor.test_yield = float(config['Sensors']['canada_test_yield'])
CarltonSensor.ReadSensorDeliverySchedule('csv/canada_sensor_delivery_schedule.csv')
env.process(CarltonSensor.TestSensors())
CarltonSensor.n_hccs.put(100000)
env.process(CarltonSensor.TestABCs(int(config['Sensors']['abc_test_rate']), float(config['Sensors']['abc_test_yield'])))

CzechSensor = endcap.SensorTestSite(env, 'CzechSensor')
CzechSensor.test_rate = float(config['Sensors']['czech_test_rate'])
CzechSensor.test_yield = float(config['Sensors']['czech_test_yield'])
CzechSensor.ReadSensorDeliverySchedule('csv/czech_sensor_delivery_schedule.csv')
env.process(CzechSensor.TestSensors())

FreiburgBareHybrid = endcap.RawPartMaker(env, 'FreiburgBareHybrid')
list_of_bare_hybrid_sites = [FreiburgBareHybrid]
endcap.ReadEndcapBareHybridRates(env, list_of_bare_hybrid_sites, 'csv/endcap_bare_hybrid_rates.csv')

FreiburgPowerboard = endcap.RawPartMaker(env, 'FreiburgPowerboard')
list_of_powerboard_sites = [FreiburgPowerboard]
endcap.ReadEndcapPowerboardRates(env, list_of_powerboard_sites, 'csv/endcap_powerboard_rates.csv')

## Barrel

JapanSensor = barrel.SensorTestSite(env, 'JapanSensor')
JapanSensor.test_rate = float(config['Sensors']['japan_test_rate'])
JapanSensor.test_yield = float(config['Sensors']['japan_test_yield'])
JapanSensor.ReadSensorDeliverySchedule('csv/japan_sensor_delivery_schedule.csv')
env.process(JapanSensor.TestSensors())

UKSensor = barrel.SensorTestSite(env, 'UKSensor')
UKSensor.test_rate = float(config['Sensors']['uk_test_rate'])
UKSensor.test_yield = float(config['Sensors']['uk_test_yield'])
UKSensor.ReadSensorDeliverySchedule('csv/uk_sensor_delivery_schedule.csv')
env.process(UKSensor.TestSensors())
UKSensor.n_abcs.put(1000000)
UKSensor.n_hccs.put(1000000)

BarrelRawPartMaker = barrel.RawPartMaker(env, 'BarrelRawPartMaker')
BarrelRawPartMaker.ls_target = float(config['Barrel Raw Parts']['raw_part_ls_target'])
BarrelRawPartMaker.bare_hybrid_ls_rate = float(config['Barrel Raw Parts']['bare_hybrid_ls_rate'])
BarrelRawPartMaker.bare_hybrid_ss_rate = float(config['Barrel Raw Parts']['bare_hybrid_ss_rate'])
BarrelRawPartMaker.powerboard_ls_rate = float(config['Barrel Raw Parts']['powerboard_ls_rate'])
BarrelRawPartMaker.powerboard_ss_rate = float(config['Barrel Raw Parts']['powerboard_ss_rate'])
env.process(BarrelRawPartMaker.MakeBareHybrids())
env.process(BarrelRawPartMaker.MakePowerboards())

# Set up production sites

## Endcap

Hamburg = endcap.ProductionSite(env, 'Hamburg')
Toronto = endcap.ProductionSite(env, 'Toronto')
Zeuthen = endcap.ProductionSite(env, 'Zeuthen')
Scandinavia = endcap.ProductionSite(env, 'Scandinavia')
Dortmund = endcap.ProductionSite(env, 'Dortmund')
Freiburg = endcap.ProductionSite(env, 'Freiburg')
Valencia = endcap.ProductionSite(env, 'Valencia')
Prague = endcap.ProductionSite(env, 'Prague')
Vancouver = endcap.ProductionSite(env, 'Vancouver')
Australia = endcap.ProductionSite(env, 'Australia')

endcap_sites = [Hamburg, Toronto, Zeuthen, Scandinavia, Dortmund, Freiburg, Valencia, Prague, Vancouver, Australia]

## Barrel

BNL = barrel.ProductionSite(env, 'BNL')
LBL = barrel.ProductionSite(env, 'LBL')
SCIPP = barrel.ProductionSite(env, 'SCIPP')
Birmingham = barrel.ProductionSite(env, 'Birmingham')
Cambridge = barrel.ProductionSite(env, 'Cambridge')
Glasgow = barrel.ProductionSite(env, 'Glasgow')
Liverpool = barrel.ProductionSite(env, 'Liverpool')
Sheffield = barrel.ProductionSite(env, 'Sheffield')
RAL = barrel.ProductionSite(env, 'RAL')
IHEP = barrel.ProductionSite(env, 'IHEP')

barrel_sites = [BNL, LBL, SCIPP, Birmingham, Cambridge, Glasgow, Liverpool, Sheffield, RAL, IHEP]

ProductionSiteDictionary = {
    'Hamburg':Hamburg,
    'Toronto':Toronto,
    'Zeuthen':Zeuthen,
    'Scandinavia':Scandinavia,
    'Dortmund':Dortmund,
    'Freiburg':Freiburg,
    'Valencia':Valencia,
    'Prague':Prague,
    'Vancouver':Vancouver,
    'Australia':Australia,
    'CarltonSensor':CarltonSensor,
    'CzechSensor':CzechSensor,
    'FreiburgPowerboard':FreiburgPowerboard,
    'FreiburgBareHybrid':FreiburgBareHybrid,
    'BNL':BNL,
    'LBL':LBL,
    'SCIPP':SCIPP,
    'Birmingham':Birmingham,
    'Cambridge':Cambridge,
    'Glasgow':Glasgow,
    'Liverpool':Liverpool,
    'Sheffield':Sheffield,
    'RAL':RAL,
    'IHEP':IHEP,
    'JapanSensor':JapanSensor,
    'UKSensor':UKSensor,
    'BarrelRawPartMaker':BarrelRawPartMaker
}

# Read in module and hybrid production rates

## Endcap

endcap_module_yields = []
endcap_hybrid_yields = []

endcap.ReadEndcapModuleYields('csv/endcap_module_yields.csv', endcap_sites, endcap_module_yields)
endcap.ReadEndcapHybridYields('csv/endcap_hybrid_yields.csv', endcap_sites, endcap_hybrid_yields)

endcap.ReadEndcapModuleRates('csv/endcap_module_rates.csv', endcap_sites, endcap_module_yields)
endcap.ReadEndcapHybridRates('csv/endcap_hybrid_rates.csv', endcap_sites, endcap_hybrid_yields, endcap_module_yields)

## Barrel

barrel_module_yields = []
barrel_hybrid_yields = []
    
barrel.ReadBarrelYields('csv/barrel_yields.csv', barrel_sites, barrel_module_yields, barrel_hybrid_yields)
barrel.ReadBarrelModuleRates('csv/barrel_module_rates.csv', barrel_sites)
barrel.ReadBarrelHybridRates('csv/barrel_hybrid_rates.csv', barrel_sites)

# Schedule production processes

## Module and hybrid production

for site in endcap_sites:
    env.process(site.MakeParts())
for site in barrel_sites:
    env.process(site.MakeParts())

## Hybrid burn-in

env.process(Hamburg.HybridBurnIn(float(config['Burn-In']['hamburg_burn_in_capacity'])))
env.process(Toronto.HybridBurnIn(float(config['Burn-In']['toronto_burn_in_capacity'])))
env.process(Scandinavia.HybridBurnIn(float(config['Burn-In']['scandinavia_burn_in_capacity'])))
env.process(Zeuthen.HybridBurnIn(float(config['Burn-In']['zeuthen_burn_in_capacity'])))
env.process(Freiburg.HybridBurnIn(float(config['Burn-In']['freiburg_burn_in_capacity'])))
env.process(Dortmund.HybridBurnIn(float(config['Burn-In']['dortmund_burn_in_capacity'])))

env.process(BNL.HybridBurnIn(float(config['Burn-In']['bnl_burn_in_capacity'])))
env.process(LBL.HybridBurnIn(float(config['Burn-In']['lbl_burn_in_capacity'])))
env.process(SCIPP.HybridBurnIn(float(config['Burn-In']['scipp_burn_in_capacity'])))
env.process(Birmingham.HybridBurnIn(float(config['Burn-In']['birmingham_burn_in_capacity'])))
env.process(Liverpool.HybridBurnIn(float(config['Burn-In']['liverpool_burn_in_capacity'])))
env.process(IHEP.HybridBurnIn(float(config['Burn-In']['ihep_burn_in_capacity'])))

# Petals and staves

env.process(Hamburg.MakePetal(float(config['Petals']['hamburg_days_to_make_petal']),0,float(config['Petals']['hamburg_petal_yield'])))
env.process(Freiburg.MakePetal(float(config['Petals']['freiburg_days_to_make_petal']),0,float(config['Petals']['freiburg_petal_yield'])))
env.process(Vancouver.MakePetal(float(config['Petals']['vancouver_days_to_make_petal']),0,float(config['Petals']['vancouver_petal_yield'])))
env.process(Valencia.MakePetal(float(config['Petals']['valencia_days_to_make_petal']),0,float(config['Petals']['valencia_petal_yield'])))

env.process(BNL.MakeStaves(float(config['Staves']['bnl_days_to_make_stave']),float(config['Staves']['bnl_stave_yield'])))
env.process(RAL.MakeStaves(float(config['Staves']['ral_days_to_make_stave']),float(config['Staves']['ral_stave_yield'])))


# Set up shipping

model.ReadShipping(env, 'csv/endcap_shipping.csv', ProductionSiteDictionary)
model.ReadShipping(env, 'csv/barrel_shipping.csv', ProductionSiteDictionary)

# Get current inventories and clear tracking variables

for key in ProductionSiteDictionary:
    collector = model.DataCollector(env, ProductionSiteDictionary[key])
    env.process(collector.GetInventory())

for key in ProductionSiteDictionary:
    env.process(ProductionSiteDictionary[key].ClearItemsMade())

# Schedule delays

if config.getboolean('Delays', 'schedule_delay') == True:
    parsed_list_of_delays = []
    for line in list(filter(None, (x.strip() for x in config['Delays']['list_of_scheduled_delays'].splitlines()))):
        entry = line.split(',')
        entry[1] = int(entry[1])
        entry[2] = int(entry[2])
        parsed_list_of_delays.append(entry)
    for entry in parsed_list_of_delays:
        env.process(model.ScheduleSiteShutdown(env, ProductionSiteDictionary[entry[0]], entry[1], entry[2]))
    
# Run simulation

model.OUTPUTDIR = config['General']['output_dir']

env.process(model.PrintProgress(env,100))

stop_date = config.getint('General', 'days_to_run')

env.run(until=stop_date)

# Make plots

print('Making Plots')

model.START_OFFSET = 60
model.StaticMakePlots(endcap_sites)
model.StaticMakePlots(barrel_sites)
